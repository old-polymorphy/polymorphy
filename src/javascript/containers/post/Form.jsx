import { connect } from 'react-redux';
import PostFormComponent from '../../components/post/Form';
import { setCalendar } from '../../store/actions';

const mapDispatchToProps = (dispatch) => {
  return {
    setCalendar(calendar) {
      dispatch(setCalendar(calendar));
    }
  };
};

export default connect(null, mapDispatchToProps)(PostFormComponent);