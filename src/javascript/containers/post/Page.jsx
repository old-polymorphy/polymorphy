// Redux
import { connect } from 'react-redux';

// Components
import PostPageComponent from '../../components/post/Page';

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};

export default connect(mapStateToProps)(PostPageComponent);