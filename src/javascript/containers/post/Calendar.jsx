import { connect } from 'react-redux';
import PostCalendarComponent from '../../components/post/Calendar';

const mapStateToProps = (state) => {
  return {
    calendar: state.calendar
  };
};

export default connect(mapStateToProps)(PostCalendarComponent);