import { connect } from 'react-redux';
import PostComponent from '../../components/post/Post';
import { setCalendar } from '../../store/actions';

const mapStateToProps = (state) => {
  return {
    user: state.user
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setCalendar(calendar) {
      dispatch(setCalendar(calendar));
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostComponent);