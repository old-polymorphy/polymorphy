// Libs
import $ from 'jquery';
import axios from 'axios';
import 'jquery-mask-plugin';

// Polyfills
import '@babel/polyfill';

// Styles
import '../sass/style.sass';

// Custom
import './custom';

/**
 * Checks offset.
 *
 * @returns {void}
 */
function checkOffset() {
  if (pageYOffset > 0) {
    $('.up').fadeIn(125);
    if (mq.matches) {
      $('.logo-wrapper').removeClass('logo-wrapper-top');
    }
  } else {
    $('.up').fadeOut(125);
    if (mq.matches) {
      $('.logo-wrapper').addClass('logo-wrapper-top');
    }
  }
}

/**
 * Shows a message.
 *
 * @returns {void}
 */
function popUp(message) {
  $('.popup span').text(message);
  $('.popup').animate({
    bottom: '40px',
    opacity: '1'
  }, 500)
    .delay(1500)
    .animate({
      bottom: '-100px',
      opacity: '0'
    }, 500);
}

/**
 * Sends an Axios request.
 *
 * @param {string} path URL to send the form.
 * @param {FormData} data form data.
 *
 * @returns {void}
 */
function sendForm(path, data) {
  let button = $(this).find('input:last-child');

  button.val('Секунду...');

  axios
    .post(`/${path}`, data, {
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-Token': token
      }
    })
    .then(() => {
      popUp('Отправлено!');
    })
    .catch(() => {
      popUp('Произошла ошибка');
    })
    .finally(() => {
      button.val('Отправить');

      $('.fade').fadeOut(125);
    });
}

// Mobile media query
const mq = window.matchMedia('(max-width: 864px)');

// Get CSRF fields
const param = $('meta[name=csrf-param]').attr('content');
const token = $('meta[name=csrf-token]').attr('content');

$(document).ready(() => {
  // Masked input
  $(() => {
    $('#phone, #callback-phone').mask('8(999) 999-9999');
  });

  // Check offset
  checkOffset();

  // Open callback form
  $('.homePage header button').on('click', () => {
    $('div.background, form.callback').fadeIn(125);
  });

  // Background click
  $('div.background').on('click', () => {
    if (!$('.cross').length) {
      $('.fade').fadeOut(125);
    }
  });

  // Navigation scroll
  $('.homePage nav')
    .find('a')
    .on('click', function () {
      const destination = $(this).attr('name');
      let scroll = $('.' + destination).offset().top;
      if (mq.matches) {
        scroll -= 64;
      }
      $('html, body').animate({
        scrollTop: scroll
      }, 500);
      $('.cross').trigger('click');
    });

  // Order scroll
  $('.homePage div.prices button').on('click', () => {
    $('html, body').animate({
      scrollTop: $('div.order').offset().top
    }, 500);
    $('div.order input:first-of-type').focus();
  });

  // "Up" button
  $('.up').on('click', () => {
    $('html, body').animate({
      scrollTop: 0
    }, 500);
  });

  // Ask to call back.
  $('form.callback').on('submit', function (event) {
    event.preventDefault();

    // Get fields and the button.
    const phone = $(this)
      .find('input:first-child')
      .val();

    // Form data
    let data = new FormData();
    data.append(param, token);
    data.append('phone', phone);

    sendForm.apply(this, ['callback', data]);
  });

  // Make an order.
  $('.homePage .order').on('submit', function (event) {
    event.preventDefault();
    event.stopImmediatePropagation();

    // Get fields and the button.
    const name = $(this).find('input#name').val();
    const email = $(this).find('input#email').val();
    const phone = $(this).find('input#phone').val();
    const order = $(this).find('textarea#order').val();

    // Form data
    let data = new FormData();
    data.append(param, token);
    data.append('name', name);
    data.append('email', email);
    data.append('phone', phone);
    data.append('order', order);

    sendForm.apply(this, ['order', data]);
  });

  // Lines
  $('.lines').on('click', function () {
    if ($(this).hasClass('cross')) {
      $(this)
        .addClass('lines')
        .removeClass('cross');
      $('header nav').css('right', '');
      $('div.background').fadeOut(125);
    } else {
      $(this).addClass('cross');
      $('header nav').css('right', '30px');
      $('div.background').fadeIn(125);
    }
  });
});

// Scroll hook.
window.onscroll = (() => {
  checkOffset();
});
