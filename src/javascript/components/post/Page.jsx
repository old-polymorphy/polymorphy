import React from 'react';

// Components
import Tag from '../Tag';
import PostListComponent from './List';

// Component containers
import PostFormContainer from '../../containers/post/Form';
import PostCalendarContainer from '../../containers/post/Calendar';

// Providers
import postsService from '../../services/posts';
import tagService from '../../services/tag';

// Libs
import createHistory from 'history/createBrowserHistory';

/**
 * PostsPage class.
 *
 * @extends React.Component
 */
export default class PostPageComponent extends React.Component {
  /**
   * Class constructor.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);

    this.state = {
      activeTags: [],
      editedPost: null,
      loading: false,
      posts: [],
      showAddForm: false,
      showEditForm: false,
      tags: [],
    };

    this.setHistoryListener();
  }

  /** @inheritDoc */
  render() {
    return (
      <div className="container">
        <h1 className="title is-1">Блог компании</h1>

        {/* Add post button */}
        {this.renderAddPostButton()}

        {/* Add form */}
        <div>
          <PostFormContainer
            close={this.handleAddForm}
            handleAddPost={this.handleAddPost}
            show={this.state.showAddForm}
            tags={this.state.tags}
            type={'add'}
          />
        </div>

        {/* Edit form */}
        <div>
          <PostFormContainer
            close={this.closeEditForm}
            handleEditPost={this.handleEditPost}
            postId={this.state.editedPost}
            show={this.state.showEditForm}
            tags={this.state.tags}
            type={'edit'}
          />
        </div>

        <div className="tags activeTags">{this.renderActiveTags()}</div>

        <div className="columns">

          {/* Posts column */}
          <div className="column is-four-fifths">

            {/* Loader */}
            {this.renderLoader()}

            {/* Posts list */}
            <PostListComponent
              activate={this.activateTag}
              handleDeletePost={this.handleDeletePost}
              handleEditForm={this.handleEditForm}
              posts={this.state.posts}
              tags={this.state.activeTags}
            />

          </div>

          {/* Menu column */}
          <div className="column is-one-fifth postsMenu">

            {/* Tags */}
            <div className="box">
              <div className="postsMenu-block">
                <span>Тэги</span>
                <div className="tags">{this.renderTags()}</div>
              </div>
            </div>

            {/* Calendar */}
            {this.renderCalendar()}

          </div>
        </div>
      </div>
    );
  }

  /**
   * Renders tags.
   *
   * @returns {*[]}
   */
  renderTags = () => {
    if (this.state.activeTags.length !== this.state.tags.length) {
      let inactiveTags = this.state.tags.filter(inactiveTag => {
        let activeTags = this.state.activeTags.map(activeTag => {
          return activeTag.id;
        });

        return !activeTags.includes(inactiveTag.id);
      });

      return inactiveTags.map(tag => {
        return <Tag key={tag.id} handler={this.activateTag} data={tag}/>;
      });
    }

    return <span>Вы применили все фильтры. :)</span>;
  };

  /**
   * Renders active tags.
   *
   * @returns {*[]|*}
   */
  renderActiveTags = () => {
    if (this.state.activeTags.length) {
      return this.state.activeTags.map(tag => {
        return (
          <Tag handler={this.clearTag} key={tag.id} delete={true} data={tag}/>
        );
      });
    }

    return <span>Нет применённых фильтров</span>;
  };

  /**
   * Renders a calendar.
   *
   * @returns {*}
   */
  renderCalendar = () => {
    return (
      <PostCalendarContainer handleCalendar={this.handleCalendar}/>
    );
  };

  /**
   * Renders add post button.
   *
   * @returns {*}
   */
  renderAddPostButton = () => {
    if (this.props.user.type === 4) {
      return (
        <button
          onClick={this.handleAddForm}
          className="button is-info postsPage-addPostButton"
        >
          Добавить пост
        </button>
      );
    }
  };

  /**
   * Renders the spinner.
   *
   * @returns {ReactDOM}
   */
  renderLoader = () => {
    if (this.state.loading) {
      return (
        <div className="lds-ellipsis">
          <div/>
          <div/>
          <div/>
          <div/>
        </div>
      );
    }
  };

  /**
   * Handles calendar clicking.
   *
   * @param {string} url
   *
   * @returns {void}
   */
  handleCalendar = (url) => {
    postsService.all(url)
      .then((response) => this.setState({posts: response.data}))
      .catch((err) => console.error(err));
  };

  /**
   * Toggles add post form.
   *
   * @returns {void}
   */
  handleAddForm = () => this.setState({showAddForm: !this.state.showAddForm});

  /**
   * Toggles add post form.
   *
   * @returns {Function}
   */
  handleEditForm = (postId) => () => {
    this.setState({
      editedPost: postId,
      showEditForm: !this.state.showEditForm
    });
  };

  /**
   * Handles post deleting.
   *
   * @returns {void}
   */
  handleEditPost = () => this.fetchPosts();

  /**
   * Handles post deleting.
   *
   * @param {number} postId — Post numeric ID.
   * @returns {void}
   */
  handleDeletePost = (postId) => {
    const posts = this.state.posts.filter((post) => post.id !== postId);

    this.setState({posts});
  };

  /**
   * Handles post adding.
   *
   * @param {*} post — New post object.
   * @returns {void}
   */
  handleAddPost = (post) => {
    this.setState({
      posts: [post, ...this.state.posts]
    });
  };

  /**
   * Sets history listener.
   *
   * @returns {void}
   */
  setHistoryListener = () => {
    this.history = createHistory();

    this.history.listen(() => {
      postsService.all(`api${location.pathname}`)
        .then((response) => this.setState({posts: response.data}))
        .catch((err) => console.error(err));
    });
  };

  /**
   * Closes edit form.
   *
   * @returns {void}
   */
  closeEditForm = () => this.setState({showEditForm: false});

  /**
   * Activates the tag.
   *
   * @param {object} tag
   *
   * @returns {void}
   */
  activateTag = (tag) => {
    tag = tag.props.data;

    if (!this.state.activeTags.includesObject(tag.id)) {
      this.setState(prevState => ({
        activeTags: prevState.activeTags.concat(tag)
      }), this.filter);
    }
  };

  /**
   * Clears the tag.
   *
   * @param {Tag} tag
   *
   * @returns {void}
   */
  clearTag = (tag) => {
    tag = tag.props.data;

    let newActiveTags = this.state.activeTags;
    let index = Tag.getIndex(newActiveTags, tag);

    newActiveTags.splice(index, 1);

    this.setState({activeTags: newActiveTags}, this.filter);
  };

  /**
   * Filters posts by the active tags.
   *
   * @returns {void}
   */
  filter = () => {
    let tags = this.state.activeTags;

    let queryString = Object.keys(tags)
      .map((key, index) => index + '=' + tags[key].id)
      .join('&');

    const link = location.href.replace('/posts', '/api/posts');

    postsService.filter(link, queryString)
      .then((response) => this.setState({posts: response.data}))
      .catch((err) => console.error(err))
      .finally(() => this.setState({loading: false}));

  };

  /**
   * Fetches all posts.
   *
   * @returns {void}
   */
  fetchPosts = () => {
    postsService.all(`api${location.pathname}`)
      .then((response) => this.setState({posts: response.data}))
      .catch((err) => console.error(err))
      .finally(() => this.setState({loading: false}));
  };

  /**
   * Fetches all tags.
   *
   * @returns {void}
   */
  fetchTags = () => {
    tagService.all('/api/tags/1')
      .then((response) => this.setState({tags: response.data}))
      .catch((err) => console.error(err));
  };

  /** @inheritDoc */
  componentWillMount() {
    this.setState({loading: true});

    this.fetchPosts();
    this.fetchTags();
  }
}