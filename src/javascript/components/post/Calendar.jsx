// React
import React from 'react';

// Libs
import moment from 'moment';

/**
 * Calendar class.
 *
 * @extends React.Component
 */
export default class PostCalendarComponent extends React.Component {
  /** @inheritDoc */
  render() {
    if (!this.props.calendar.length) {
      return null;
    }

    return (
      <div className="box">
        {this.renderYears()}
      </div>
    );
  }

  /**
   * Handles calendar click.
   *
   * @param {object} e — Event object.
   * @returns {void}
   */
  handleCalendar = (e) => {
    e.preventDefault();

    const href = e.target.pathname;

    history.pushState({}, null, href);

    this.props.handleCalendar('api' + href);
  };

  /**
   * Renders years list.
   *
   * @returns {string}
   */
  renderYears = () => {
    return this.props.calendar.map((element) => {
      return (
        <div key={element.year} className="postsMenu-block">
          <span>
            <a onClick={this.handleCalendar} href={`/posts/${element.year}`}>
              {element.year}
            </a>
          </span>
          <ul>{this.renderMonths(element.months, element.year)}</ul>
        </div>
      );
    });
  };

  /**
   * Renders months list.
   *
   * @param {array} months — Months posts were created in.
   * @param {number} year — Posts year.
   * @returns {*}
   */
  renderMonths = (months, year) => {
    moment.locale('ru');

    return months.map((month) => {
      return (
        <li key={month}>
          <a onClick={this.handleCalendar} href={`/posts/${year}/${month}`}>
            {moment.months(month - 1)}
          </a>
        </li>
      );
    });
  };
}
