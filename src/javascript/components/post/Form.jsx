// React
import React from 'react';

// Libs
import classNames from 'classnames';

// Providers
import postService from '../../services/posts.js';

/**
 * AddPostForm class.
 *
 * @extends React.Component
 */
export default class PostFormComponent extends React.Component {
  /**
   * Class constructor.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);

    this.state = {
      body: '',
      errors: {},
      file: '',
      pending: false,
      tags: [],
      title: '',
    };
  }

  // TODO: GET RID OF THE HOOK
  /** @inheritDoc */
  componentDidUpdate(prevState) {
    if (this.props.type === 'edit' && this.props.show && !prevState.show) {
      postService.getPostById(this.props.postId)
        .then((response) => {
          const {body, title} = {...response.data};
          const tags = response.data.tags.map((tag) => tag.id);
          this.setState({body, title, tags});
        });
    }
  }

  /** @inheritDoc */
  render() {
    return (
      <div className={`${this.props.show ? 'is-active' : ''} modal`}>

        {/* Modal background */}
        <div className="modal-background"/>

        {/* Modal */}
        <div className="modal-content">

          {/* Form */}
          <form className="postsPage-addForm" onSubmit={this.handleSubmit}>
            <input
              className={classNames('input', {'postForm-error': this.checkError('title')})}
              name="title"
              onChange={this.handleTitleChange}
              onFocus={this.clearErrors}
              placeholder="Заголовок"
              value={this.state.title}
            />

            <textarea
              className={classNames('textarea', {'postForm-error': this.checkError('body')})}
              name="body"
              onChange={this.handleBodyChange}
              onFocus={this.clearErrors}
              placeholder="Текст поста"
              value={this.state.body}
            />

            <div className="postsPage-addFormTags tags">{this.renderTags()}</div>

            {/* Field group */}
            <div className="field is-grouped">

              {/* Upload file */}
              <section className="control">
                <div className="file">
                  <label className="file-label">
                    <input
                      className="file-input"
                      type="file"
                      name="file"
                      onChange={this.handleFileChange}
                    />
                    <span className="file-cta">
                      <span className="file-label">
                        {this.state.file ? this.state.file.name : 'Файл'}
                      </span>
                    </span>
                  </label>
                </div>
              </section>

              {/* Submit */}
              <p className="control">
                <input
                  className="button"
                  type="submit"
                  value={
                    this.state.pending ? 'Отправляется...' : 'Отправить'
                  }
                />
              </p>

            </div>
          </form>
        </div>

        {/* Close modal */}
        <button
          onClick={this.handleClose}
          className="modal-close is-large"
          aria-label="close"
        />

      </div>
    );
  }

  /**
   * Renders tags checkboxes.
   *
   * @returns {*[]}
   */
  renderTags = () => {
    return this.props.tags.map((tag) => {
      return (
        <label key={tag.id} className="checkbox">
          <input
            onChange={this.handleTagChange}
            type="checkbox"
            value={tag.id}
            checked={this.state.tags.includes(tag.id)}
          />
          {tag.text}
        </label>
      );
    });
  };

  /**
   * Instantiates FormData and populates it with user input.
   *
   * @returns {FormData}
   */
  buildFormData = () => {
    let data = new FormData();

    // Appending data
    data.append('title', this.state.title);
    data.append('body', this.state.body);
    data.append('tags', JSON.stringify(this.state.tags));
    data.append('file', this.state.file);

    return data;
  };

  /**
   * Adds a post.
   *
   * @returns {void}
   */
  addPost = () => {

    // Successful response handler
    const successHandler = (response) => {
      if (response.data.success) {
        // Close the form
        this.handleClose();

        // Add the post to the list
        this.props.handleAddPost(response.data.data);

        // Redraw the calendar
        this.renewCalendar();

        // Clear errors
        this.clearErrors();
      } else {

        // Set errors
        this.setState({errors: response.data.errors});
      }
    };

    // Creating a post
    postService.create(this.buildFormData())
      .then(successHandler)
      .catch((err) => console.error(err))
      .finally(() => this.setState({pending: false}));
  };

  /**
   * Edits the post.
   *
   * @returns {void}
   */
  editPost = () => {

    // Successful response handler
    const successHandler = (response) => {
      if (response.data.success) {
        // Close the form
        this.handleClose();

        // Update the list
        this.props.handleEditPost();

        // Clear errors
        this.clearErrors();
      } else {

        // Set errors
        this.setState({errors: response.data.errors});
      }
    };

    // Updating the post
    postService.update(this.buildFormData(), this.props.postId)
      .then(successHandler)
      .catch((err) => console.error(err))
      .finally(() => this.setState({pending: false}));
  };

  /**
   * Reloads the calendar.
   *
   * @returns {void}
   */
  renewCalendar = () => {
    postService.getCalendar()
      .then((response) => this.props.setCalendar(response.data))
      .catch((err) => console.error(err));
  };

  /**
   * Checks for errors by the name.
   *
   * @param {string} errorName — Error name.
   * @return {string}
   */
  checkError = (errorName) => this.state.errors[errorName];

  /**
   * Clears errors.
   *
   * @returns {void}
   */
  clearErrors = () => this.setState({errors: {}});

  /**
   * Handles closing modal.
   *
   * @returns {void}
   */
  handleClose = () => {
    const form = document.querySelector('form.postsPage-addForm');

    this.setState({body: '', title: '', tags: []});
    form.querySelector('.file-input').value = '';

    this.props.close();
  };

  /**
   * Handles submitting.
   *
   * @param {*} e — Event object.
   * @returns {void}
   */
  handleSubmit = (e) => {
    e.preventDefault();

    this.setState({pending: true});

    if (this.props.type === 'add') {
      this.addPost();
    } else {
      this.editPost();
    }
  };

  /**
   * Handles title input.
   *
   * @param {*} e — Event object.
   * @returns {void}
   */
  handleTitleChange = (e) => this.setState({title: e.target.value});

  /**
   * Handles title input.
   *
   * @param {*} e — Event object.
   * @returns {void}
   */
  handleBodyChange = (e) => this.setState({body: e.target.value});

  /**
   * Handles tags.
   *
   * @param {*} e — Event object.
   * @returns {void}
   */
  handleTagChange = (e) => {
    e.persist();

    let tags;

    if (e.target.checked) {
      tags = [...this.state.tags, parseInt(e.target.value)];
    } else {
      tags = this.state.tags.filter((tag) => {
        return tag !== parseInt(e.target.value);
      });
    }

    this.setState({tags});
  };

  /**
   * Handles file uploading.
   *
   * @param {*} e — Event object.
   * @returns {void}
   */
  handleFileChange = (e) => this.setState({file: e.target.files[0]});
}
