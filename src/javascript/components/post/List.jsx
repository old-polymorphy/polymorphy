// React
import React from 'react';

// Components
import PostContainer from '../../containers/post/Post';

/**
 * PostList class.
 *
 * @extends React.Component
 */
export default class PostListComponent extends React.Component {
  /**
   * Class constructor.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);

    this.state = {
      limit: 5
    };
  }

  /** @inheritDoc */
  render() {
    return (
      <div>
        {this.renderPosts()}
        {this.renderMoreButton()}
      </div>
    );
  }

  /**
   * Renders posts.
   *
   * @returns {*[]|*}
   */
  renderPosts = () => {
    if (!this.props.posts) {
      return <div>Ничего не найдено. :(</div>;
    }

    return this.props.posts.slice(0, this.state.limit).map(post => {
      return (
        <PostContainer
          activate={this.props.activate}
          data={post}
          handleDeletePost={this.props.handleDeletePost}
          handleEditForm={this.props.handleEditForm}
          key={post.id}
        />
      );
    });
  };

  /**
   * Renders the "More" button.
   *
   * @returns {null|*}
   */
  renderMoreButton = () => {
    if (this.state.limit >= this.props.posts.length) {
      return null;
    }

    return (
      <button
        className="button is-info moreButton"
        onClick={this.handleMore}
      >
        Ещё!
      </button>
    );
  };

  /**
   * Handles "More" button click.
   *
   * @returns {void}
   */
  handleMore = () => {
    this.setState((prevState) => {
      return {limit: prevState.limit + 5};
    });
  };
}
