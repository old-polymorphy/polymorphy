// React
import React from 'react';

// Libs
import isAbsoluteUrl from 'is-absolute-url';
import moment from 'moment';

// Components
import Tag from '../Tag';

// Services
import postService from '../../services/posts';

/**
 * Post class.
 *
 * @extends React.Component
 */
export default class PostComponent extends React.Component {
  /**
   * Class constructor.
   *
   * @param {any} props
   */
  constructor(props) {
    super(props);

    this.state = {
      loaded: false
    };
  }

  /**
   * Renders post.
   *
   * @return {*}
   */
  render() {
    return (
      <div className="box postElement">
        <article className="media">
          {/* Image */}
          <div className="media-left is-hidden-mobile">
            <figure className="postElement-avatar image is-64x64">
              <img src="/images/logo.png" alt="Image"/>
            </figure>
          </div>

          {/* Content */}
          <div className="media-content">

            {/* Buttons*/}
            {this.renderAdminPanel()}

            <h2 className="title subtitle">{this.props.data.title}</h2>

            <span className="postElement-body">{this.props.data.body}</span>

            {/* Image */}
            {this.renderImage()}

            <small className="postElement-time">
              Опубликовано{' '}
              <a href="#" className="postElement-author">
                @{this.props.data.user.username}
              </a>{' '}
              {this.getTime()}
            </small>

            {/* Tags */}
            {this.renderTags()}
          </div>
        </article>
      </div>
    );
  }

  /** @inheritDoc */
  componentDidMount() {
    this.checkImageLoad(this.props.data.image);
  }

  /** @inheritDoc */
  componentDidUpdate(prevProps) {
    if (!prevProps.data.image && this.props.data.image) {
      this.checkImageLoad(this.props.data.image);
    }
  }

  /**
   * Renders tags.
   *
   * @return {*}
   */
  renderTags = () => {
    let tags = this.props.data.tags || [];

    tags = tags.map(tag => {
      return <Tag handler={this.props.activate} key={tag.id} data={tag}/>;
    });

    if (tags.length) {
      return <div className="tags">{tags}</div>;
    }
  };

  /**
   * Renders post image.
   *
   * @return {null|*}
   */
  renderImage = () => {
    let imageStyle;

    if (!this.props.data.image) {
      return null;
    }

    if (isAbsoluteUrl(this.props.data.image)) {
      imageStyle = {
        backgroundImage: `url(${this.props.data.image})`
      };
    } else {
      imageStyle = {
        backgroundImage: `url(/uploads/${this.props.data.image})`
      };
    }

    if (this.props.data.image) {
      return (
        <div
          className={
            (this.state.loaded ? '' : 'ph-item ') + 'postElement-imageWrapper'
          }
        >
          <div className="postElement-image ph-picture" style={imageStyle}/>
        </div>
      );
    }
  };

  /**
   * Renders the admin panel.
   *
   * @return {null|*}
   */
  renderAdminPanel = () => {
    if (this.props.user.id !== this.props.data.user.id) {
      return null;
    }

    return (
      <div className="postElement-buttons">

        {/* Edit button */}
        {this.renderEditButton()}

        {/* Delete button */}
        {this.renderDeleteButton()}

      </div>
    );
  };

  /**
   * Renders delete button.
   *
   * @returns {null|*}
   */
  renderDeleteButton = () => {
    if (this.props.user.id !== this.props.data.user.id) {
      return null;
    }

    return (
      <div onClick={this.handleDelete} className="postElement-deleteButton">
        &#x2715;
      </div>
    );
  };

  /**
   * Renders edit button.
   *
   * @returns {null|*}
   */
  renderEditButton = () => {
    const data = this.props.data;

    if (this.props.user.id !== data.user.id) {
      return null;
    }

    return (
      <div onClick={this.props.handleEditForm(data.id)} className="postElement-editButton">
        &sdot;&sdot;&sdot;
      </div>
    );
  };

  /**
   * Deletes the post.
   *
   * @returns {void}
   */
  handleDelete = () => {
    const postId = this.props.data.id;

    postService.delete(postId)
      .then(() => {
        this.props.handleDeletePost(postId);
        return postService.getCalendar();
      })
      .then((response) => this.props.setCalendar(response.data))
      .catch((err) => console.error(err));
  };

  /**
   * Returns readable time.
   *
   * @returns {string}
   */
  getTime = () => {
    moment.locale('ru');

    return moment(this.props.data.created_at).format('ll');
  };

  /**
   * Checks whether the image is loaded.
   *
   * @param {string} src
   *
   * @returns {void}
   */
  checkImageLoad = (src) => {
    if (!this.props.data.image) {
      return;
    }

    if (!isAbsoluteUrl(this.props.data.image)) {
      src = '/uploads/' + src;
    }

    if (this.props.data.image) {
      this.img = new Image();
      this.img.onload = () => this.setState({loaded: true});
      this.img.src = src;
    }
  };
}
