// React
import React from 'react';
import PropTypes from 'prop-types';

/**
 * Tag class.
 *
 * @extends React.Component
 */
class Tag extends React.Component {
  /**
   * Class constructor.
   *
   * @param {*} props
   *
   * @returns {void}
   */
  constructor(props) {
    super(props);

    this.clickHandler = this.clickHandler.bind(this);
  }

  /**
   * Searches for the tag and returns its index.
   *
   * @param {[]} array
   * @param {object} tag
   *
   * @returns {number}
   */
  static getIndex(array, tag) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].id === tag.id) {
        return i;
      }
    }
  }

  /**
   * Handles tag click.
   *
   * @returns {void}
   */
  clickHandler() {
    this.props.handler(this);
  }

  /**
   * Renders delete button if the tag is active.
   *
   * @returns {ReactDOM | string}
   */
  renderDeleteButton() {
    if (this.props.delete) {
      return <button className="delete is-small"/>;
    }

    return '';
  }

  /**
   * Renders a tag.
   *
   * @returns {ReactDOM}
   */
  render() {
    const tag = this.props.data;

    return (
      <a onClick={this.clickHandler} className="tag is-info">
        <span>{tag.text}</span>
        {this.renderDeleteButton()}
      </a>
    );
  }
}

// Properties types.
Tag.propTypes = {
  delete: PropTypes.bool,
  handler: PropTypes.func
};

// Default values for properties.
Tag.defaultProps = {
  delete: false
};

export default Tag;
