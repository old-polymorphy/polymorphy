// React
import React, { ReactNode } from 'react';
import PropTypes from 'prop-types';

// Components
import PortfolioListComponent from './Case';

/**
 * PostList class.
 *
 * @extends React.Component
 */
class PortfolioList extends React.Component {
  /** @inheritDoc */
  render() {
    return (
      <div className="portfolioTiles columns is-multiline">
        {this.renderCases()}
      </div>
    );
  }

  /**
   * Render cases of the current type.
   *
   * @returns {ReactNode}
   */
  renderCases = () => {
    const filteredCases = this.props.cases.filter(data => {
      return data.type === this.props.type;
    });

    return filteredCases.map(data => {
      return (
        <div
          key={data.id}
          className="column is-one-quarter-fullhd is-one-third-desktop is-half-tablet"
        >
          <PortfolioListComponent data={data}/>
        </div>
      );
    });
  };
}

// Properties types.
PortfolioList.propTypes = {
  data: PropTypes.array
};

export default PortfolioList;
