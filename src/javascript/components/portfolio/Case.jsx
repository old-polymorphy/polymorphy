// React
import React, { ReactNode } from 'react';

/**
 * Post class.
 *
 * @extends React.Component
 */
export default class PortfolioCaseComponent extends React.Component {
  /**
   * Class constructor.
   *
   * @param {object} props — Component props.
   */
  constructor(props) {
    super(props);

    this.state = {
      loaded: false
    };
  }

  /** @inheritDoc */
  render() {
    return this.renderCase();
  }

  /** @inheritDoc */
  componentDidMount() {
    this.checkImageLoad(this.props.data.image);
  }

  /**
   * Checks whether the image is loaded.
   *
   * @param {string} src — Image link.
   *
   * @returns {void}
   */
  checkImageLoad = (src) => {
    if (!this.props.data.image) {
      return;
    }

    let img = new Image(src);

    img.onload = () => {
      this.setState({
        loaded: true
      });
    };

    img.src = src;
  };

  /**
   * Renders a post.
   *
   * @returns {ReactNode}
   */
  renderCase = () => {
    let imageStyle = {
      backgroundImage: `url(${this.props.data.image})`
    };

    return (
      <div
        className={(this.state.loaded ? '' : 'ph-item ') + 'portfolioElement'}
      >
        <div className="portfolioElement-image ph-picture" style={imageStyle}>
          <div className="portfolioElement-contentWrapper">
            <div className="portfolioElement-content">
              <h2>{this.props.data.title}</h2>

              <a
                href={`/portfolio/case/${this.props.data.id}`}
                className="button"
              >
                Открыть кейс
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  };
}
