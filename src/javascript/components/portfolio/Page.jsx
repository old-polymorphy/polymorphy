import React from 'react';

// Components
import PortfolioList from './List';

// Providers
import portfolioProvider from '../../services/portfolio.js';

/**
 * PostsPage class.
 *
 * @extends React.Component
 */
export default class PortfolioPageComponent extends React.Component {
  /**
   * Class constructor.
   *
   * @param {*} props
   */
  constructor(props) {
    super(props);

    this.state = {
      cases: [],
      loading: false,
      type: 1,
    };
  }

  /** @inheritDoc */
  render() {
    return (
      <div>
        {this.state.loading}

        <nav className="portfolioNavigation">
          <a
            href="#"
            data-type="1"
            onClick={this.handleTab}
            className={this.checkTabType(1)}
          >
            сайты
          </a>
          <a
            href="#"
            data-type="2"
            onClick={this.handleTab}
            className={this.checkTabType(2)}
          >
            приложения
          </a>
          <a
            href="#"
            data-type="3"
            onClick={this.handleTab}
            className={this.checkTabType(3)}
          >
            графика
          </a>
        </nav>
        <PortfolioList type={this.state.type} cases={this.state.cases}/>
      </div>
    );
  }

  /** @inheritDoc */
  componentWillMount() {
    this.setState({
      loading: true
    });

    portfolioProvider.getAll()
      .then((response) => {
        this.setState({cases: response.data});
      })
      .catch((err) => {
        console.error(err.response.statusText);
      })
      .finally(() => {
        this.setState({loading: false});
      });
  }

  /**
   * Checks which tab is active.
   *
   * @param {number} type — Cases type.
   *
   * @returns {string}
   */
  checkTabType = (type) => {
    if (this.state.type === type) {
      return 'portfolioNavigation-element-active portfolioNavigation-element';
    }

    return 'portfolioNavigation-element';
  };

  /**
   * Handles tab click.
   *
   * @param {Event} e — Event object.
   *
   * @returns {void}
   */
  handleTab = (e) => {
    const type = e.target.dataset.type;

    this.setState({type: parseInt(type)});
  };
}