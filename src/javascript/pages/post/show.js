import React from 'react';
import ReactDOM from 'react-dom';

// Components
import PostsPageContainer from '../../containers/post/Page';

// Store
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducers from '../../store/reducers';
import { setUser, setPosts, setCalendar } from '../../store/actions';

// Providers
import userProvider from '../../services/user.js';
import postProvider from '../../services/posts.js';

const initialState = {};

const store = createStore(
  reducers, initialState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

/**
 * Getting the user.
 *
 * @returns {void}
 */
userProvider.get()
  .then((data) => {
    store.dispatch(setUser(data));
  })
  .catch((e) => {
    console.error(e);
  });

/**
 * Getting the user.
 *
 * @returns {void}
 */
// postProvider.getAll(`api${location.pathname}`)
//   .then((data) => {
//     store.dispatch(setPosts(data));
//   })
//   .catch((e) => {
//     console.error(e);
//   });

/**
 * Getting the calendar.
 *
 * @returns {void}
 */
postProvider.getCalendar()
  .then((response) => store
    .dispatch(setCalendar(response.data)))
  .catch((e) => console.error(e));

ReactDOM.render(
  <Provider store={store}>
    <PostsPageContainer/>
  </Provider>,
  document.getElementById('postsPage')
);