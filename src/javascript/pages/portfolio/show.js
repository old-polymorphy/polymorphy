import React from 'react';
import ReactDOM from 'react-dom';

import Page from '../../components/portfolio/Page';

ReactDOM.render(<Page />, document.getElementById('portfolioPage'));