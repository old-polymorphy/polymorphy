// Libs￼
import { combineReducers } from 'redux';

// Store
import {
  SET_USER,
  SET_POSTS,
  SET_CALENDAR,
  ADD_POST,
  DELETE_POST
} from './actions';

/**
 * User reducer.
 *
 * @param {object} state
 * @param {object} action
 *
 * @returns {{user: (Object|Page.state.user|{})}}
 */
function user(state = {}, action) {
  switch (action.type) {
    case SET_USER:
      return action.user;

    default:
      return state;
  }
}

/**
 * Posts reducer.
 *
 * @param {object} state
 * @param {object} action
 *
 * @returns {*}
 */
function posts(state = [], action) {
  switch (action.type) {
    case SET_POSTS:
      return action.posts;

    case ADD_POST:
      return [action.posts, ...state];

    case DELETE_POST:
      return state.filter(post => {
        return post.id !== parseInt(action.postId);
      });

    default:
      return state;
  }
}

/**
 * Calendar reducer.
 *
 * @param {object} state
 * @param {object} action
 *
 * @returns {*}
 */
function calendar(state = [], action) {
  switch (action.type) {
    case SET_CALENDAR:
      return action.calendar;

    default:
      return state;
  }
}

const reducers = combineReducers({
  user,
  posts,
  calendar
});

export default reducers;
