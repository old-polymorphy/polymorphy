export const SET_USER = 'SET_USER';
export const SET_POSTS = 'SET_POSTS';
export const ADD_POST = 'ADD_POST';
export const DELETE_POST = 'DELETE_POST';
export const SET_CALENDAR = 'SET_CALENDAR';

/**
 * Sets a user.
 *
 * @param {object} user
 *
 * @returns {{type: string, user: *}}
 */
export function setUser(user) {
  return {type: SET_USER, user};
}

/**
 * Sets posts.
 *
 * @param {object[]} posts
 *
 * @returns {{type: string, posts: *}}
 */
export function setPosts(posts) {
  return {type: SET_POSTS, posts};
}

/**
 * Sets posts.
 *
 * @param {object[]} post
 *
 * @returns {{type: string, post: *}}
 */
export function addPost(post) {
  return {type: ADD_POST, post};
}

/**
 * Sets posts.
 *
 * @param {number} postId
 *
 * @returns {{type: string, user: *}}
 */
export function deletePost(postId) {
  return {type: DELETE_POST, postId};
}

/**
 * Sets the calendar.
 *
 * @param {object[]} calendar
 *
 * @returns {{type: string, user: *}}
 */
export function setCalendar(calendar) {
  return {type: SET_CALENDAR, calendar};
}