// CSRF token
export const token = document
  .querySelector('meta[name="csrf-token"]')
  .getAttribute('content');