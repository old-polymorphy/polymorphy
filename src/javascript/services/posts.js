// Libs
import axios from 'axios';

const token = document
  .querySelector('meta[name="csrf-token"]')
  .getAttribute('content');

export default {
  /**
   * Gets a post by the ID.
   *
   * @param {number} postId — Numeric post ID.
   * @returns {AxiosPromise<any>}
   */
  getPostById(postId) {
    return axios.get(`/api/post/${postId}`);
  },

  /**
   * Updates the post.
   *
   * @param {FormData} formData — FormData object.
   * @param {number} postId — Numeric post ID.
   * @returns {AxiosPromise<any>}
   */
  update(formData, postId) {
    return axios.post(`/api/posts/edit/${postId}`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-TOKEN': token,
      }
    });
  },

  /**
   * Gets post depending on parameters.
   *
   * @param {string} url — URL string.
   * @returns {AxiosPromise<any>}
   */
  all(url = '/api/posts') {
    return axios.get(`/${url}`);
  },

  /**
   * Gets posts calendar.
   *
   * @returns {Promise<Response>}
   */
  getCalendar() {
    return axios.get('/api/dates');
  },

  /**
   * Creates a post.
   *
   * @param {FormData} formData — FormData object.
   * @returns {AxiosPromise<Response>}
   */
  create(formData) {
    return axios.post('/post/add', formData, {
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'X-CSRF-Token': token
      }
    });
  },

  /**
   * Filters tags.
   *
   * @param link
   * @param tags
   * @returns {AxiosPromise<any>}
   */
  filter(link, tags) {
    return axios.get(`${link}/?${tags}`);
  },

  /**
   * Deletes the post.
   *
   * @param {number} postId — Numeric post ID.
   * @returns {Promise<Response>}
   */
  delete(postId) {
    return axios.get(`/api/posts/delete/${postId}`);
  }
};
