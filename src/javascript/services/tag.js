// Libs
import axios from 'axios';

export default {
  /**
   * Gets tags depending on parameters.
   *
   * @param {string} url — URL string.
   *
   * @returns {AxiosPromise<any>}
   */
  all(url) {
    return axios.get(`${url}`);
  }
};
