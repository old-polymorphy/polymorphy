import axios from 'axios';

export default {
  /**
   * Gets all cases.
   *
   * @returns {AxiosPromise<any>}
   */
  getAll() {
    return axios.get('/api/portfolio');
  }
};
