export default {
  /**
   * Gets tags depending on parameters.
   *
   * @returns {Promise<Response>}
   */
  async get() {
    const response = await fetch('/api/user');
    return await response.json();
  }
};
