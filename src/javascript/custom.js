/**
 * Checks if the array of objects contains
 * an object with the specific ID.
 *
 * @param {number} objectId
 *
 * @returns {boolean}
 */
Array.prototype.includesObject = function (objectId) {
  for (let i = 0; i < this.length; i++) {
    if (this[i].id === objectId) {
      return true;
    }
  }
};