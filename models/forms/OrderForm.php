<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class OrderForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $order;

    /**
     * Returns validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            [['name', 'email', 'phone', 'order'], 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * Generates email body.
     *
     * @return string
     */
    private function generateBody(): string
    {
        $body = "Order from polymorphy.ru\n" .
            "Name: {$this->name}\n" .
            "Email: {$this->email}\n" .
            "Phone: {$this->phone}\n" .
            "Order: {$this->order}\n";

        return $body;
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @return bool|array
     */
    public function send()
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo(env('MAIL_USER'))
                ->setFrom(env('MAIL_USER'))
                ->setSubject('Order')
                ->setTextBody($this->generateBody())
                ->send();

            return true;
        }

        return $this->errors;
    }
}
