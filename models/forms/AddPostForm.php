<?php

namespace app\models\forms;

// Yii
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\UploadedFile;

// Models
use app\models\Post;
use app\models\Tag;

/**
 * Class AddPostForm
 *
 * @category Forms
 * @package  app\models\forms
 */
class AddPostForm extends Model
{
    public $title;
    public $body;
    public $tags;

    /** @var UploadedFile */
    public $file;

    /**
     * Returns validation rules.
     *
     * @return array the validation rules.
     */
    public function rules(): array
    {
        return [
            [['title', 'body'], 'required'],
            [['tags'], 'string'],
            [['file'], 'file', 'extensions' => 'png, jpg, gif'],
        ];
    }

    /**
     * Generates a unique name.
     *
     * @return string
     */
    private function generateName(): string
    {
        return uniqid('img_', true) . '.' . $this->file->extension;
    }

    /**
     * Uploads the file.
     *
     * @param string $name
     *
     * @return bool
     */
    private function upload(string $name): bool
    {
        if ($this->validate()) {
            $this->file->saveAs('uploads/' . $name);

            return true;
        }

        return false;
    }

    /**
     * Returns the new post with tags.
     *
     * @param Post $newPost — Newly created post.
     *
     * @return array|null|ActiveRecord
     */
    private function getNewPost(Post $newPost)
    {
        return Post::find()
            ->with('tags')
            ->with('user')
            ->where(['id' => $newPost->getPrimaryKey()])
            ->asArray()
            ->one();
    }

    /**
     * Sends an email to the specified email address
     * using the information collected by this model.
     *
     * @return Post|array
     */
    public function persist()
    {
        if ($this->validate()) {
            $post = new Post();

            $post->setAttribute('title', $this->title);
            $post->setAttribute('body', $this->body);

            if (isset($this->file)) {
                $name = $this->generateName();

                if ($this->upload($name)) {
                    $post->setAttribute('image', $name);
                }
            }

            $post->save();

            foreach (Json::decode($this->tags) as $tag) {
                $post->link('tags', Tag::findOne($tag));
            }

            return ['success' => true, 'data' => $this->getNewPost($post)];
        }

        return ['success' => false, 'errors' => $this->errors];
    }
}
