<?php

namespace app\models\forms;

// Yii
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\web\UploadedFile;

// Models
use app\models\Post;
use app\models\Tag;

/**
 * Class EditPostForm
 *
 * @category Forms
 * @package  app\models\forms
 */
class EditPostForm extends Model
{
    public $id;
    public $title;
    public $body;
    public $tags;

    /** @var UploadedFile */
    public $file;

    /**
     * Returns validation rules.
     *
     * @return array the validation rules.
     */
    public function rules(): array
    {
        return [
            [['title', 'body'], 'required'],
            [['body', 'tags', 'title'], 'string'],
            [['file'], 'file', 'extensions' => 'png, jpg, gif'],
        ];
    }

    /**
     * Generates a unique name.
     *
     * @return string
     */
    private function generateName(): string
    {
        return uniqid('img_', true) . '.' . $this->file->extension;
    }

    /**
     * Uploads the file.
     *
     * @param string $name
     *
     * @return bool
     */
    private function upload(string $name): bool
    {
        if ($this->validate()) {
            $this->file->saveAs('uploads/' . $name);

            return true;
        }

        return false;
    }

    /**
     * Returns the new post with tags.
     *
     * @param Post $newPost — Newly created post.
     *
     * @return array|null|ActiveRecord
     */
    private function getNewPost(Post $newPost)
    {
        return Post::find()
            ->with('tags')
            ->with('user')
            ->where(['id' => $newPost->getPrimaryKey()])
            ->asArray()
            ->one();
    }

    private function removeFile(Post $post): void
    {
        $path = Yii::getAlias('@webroot') . '/uploads/' . $post->image;

        if ($post->image && file_exists($path)) {
            unlink($path);
        }
    }

    private function uploadFile(Post $post): void
    {
        $name = $this->generateName();

        if ($this->upload($name)) {
            $post->setAttribute('image', $name);
        }
    }

    private function unlinkTags(Post $post): void
    {
        foreach ($post->tags as $tag) {
            $post->unlink('tags', $tag, true);
        }
    }

    private function linkTags(Post $post): void
    {
        foreach (Json::decode($this->tags) as $tag) {
            $post->link('tags', Tag::findOne($tag));
        }
    }

    /**
     * Sends an email to the specified email address
     * using the information collected by this model.
     *
     * @return array|ActiveRecord|null
     */
    public function persist()
    {
        if ($this->validate()) {
            $post = Post::findOne($this->id);

            if (!$post) {
                return null;
            }

            $post->setAttribute('title', $this->title);
            $post->setAttribute('body', $this->body);

            if (isset($this->file)) {
                $this->removeFile($post);
                $this->uploadFile($post);
            }

            $this->unlinkTags($post);
            $this->linkTags($post);

            $post->save();

            return ['success' => true, 'data' => $this->getNewPost($post)];
        }

        return ['success' => false, 'errors' => $this->errors];
    }
}
