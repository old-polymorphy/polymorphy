<?php

namespace app\models\forms;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CallbackForm extends Model
{
    public $phone;

    /**
     * Returns validation rules.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            [['phone'], 'required'],
        ];
    }

    /**
     * Generates email body.
     *
     * @return string
     */
    private function generateBody(): string
    {
        $body = "Callback request from polymorphy.ru\n" .
            "Phone: {$this->phone}";

        return $body;
    }

    /**
     * Sends an email to the specified email address
     * using the information collected by this model.
     *
     * @return array|bool
     */
    public function send()
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo(env('MAIL_USER'))
                ->setFrom(env('MAIL_USER'))
                ->setSubject('Callback')
                ->setTextBody($this->generateBody())
                ->send();

            return true;
        }

        return $this->errors;
    }
}
