<?php

namespace app\models;

// Framework
use yii\base\InvalidConfigException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\ActiveQuery;
use yii\db\Expression;

// Libs
use Carbon\Carbon;

/**
 * This is the model class for table "tbl_posts".
 *
 * @property string id
 * @property string title
 * @property string body
 * @property string image
 * @property string user_id
 * @property string created_at
 * @property Tag[] tags
 */
class Post extends ActiveRecord
{
    /** @inheritdoc */
    public function rules(): array
    {
        return [
            [['title', 'body'], 'required'],
            [['image'], 'string'],
        ];
    }

    /** @inheritdoc */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
//                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /** @inheritdoc */
    public static function tableName(): string
    {
        return '{{public.posts}}';
    }

    /**
     * Gets tags.
     *
     * @return string|ActiveQuery
     */
    public function getTags()
    {
        try {
            return $this->hasMany(Tag::class, ['id' => 'tag_id'])
                ->viaTable('post_tag', ['post_id' => 'id']);
        } catch (InvalidConfigException $e) {
            return $e->getMessage();
        }
    }

    /**
     * Gets the author.
     *
     * @return ActiveQuery
     */
    public function getUser(): ActiveQuery
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Generates calendar for posts.
     *
     * @param array $dates
     *
     * @return array
     */
    public static function generateCalendar(array $dates): array
    {
        $result = [];

        foreach ($dates as $date) {
            $date = Carbon::parse($date->created_at);

            if (!array_key_exists($date->year, $result)) {
                $result[$date->year] = [];
            }

            if (!in_array($date->month, $result[$date->year], false)) {
                $result[$date->year][] = $date->month;
            }
        }

        $unsorted = [];

        foreach ($result as $key => $value) {
            $unsorted[] = [
                'year' => $key,
                'months' => $value,
            ];
        }

        return $unsorted;
    }

    /** @inheritdoc */
    public function afterDelete()
    {
        $fileName = "uploads/{$this->image}";

        if ($this->image && file_exists($fileName)) {
            unlink($fileName);
        }
    }
}