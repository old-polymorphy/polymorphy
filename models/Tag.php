<?php

namespace app\models;

use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

class Tag extends ActiveRecord
{
    /**
     * Return the name of the table associated with this ActiveRecord class.
     *
     * @return string
     */
    public static function tableName(): string
    {
        return '{{public.tags}}';
    }

    /**
     * Gets posts.
     *
     * @return InvalidConfigException|ActiveQuery
     */
    public function getPosts(): ?ActiveQuery
    {
        try {
            return $this->hasMany(Post::class, ['id' => 'post_id'])
                ->viaTable('post_tag', ['tag_id' => 'id']);
        } catch (InvalidConfigException $e) {
            return $e;
        }
    }
}