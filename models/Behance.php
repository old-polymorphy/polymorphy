<?php

namespace app\models;

use \GuzzleHttp\Client;
use phpDocumentor\Reflection\Types\Mixed_;
use Psr\Http\Message\StreamInterface;

/**
 * Class Behance
 *
 * @package app\models
 */
class Behance
{
    private $token;
    private $guzzle;

    /**
     * Behance constructor.
     *
     * @return void
     */
    public function __construct()
    {
        $this->token = env('BEHANCE_API_KEY');
        $this->guzzle = new Client();
    }

    /**
     * Gets project info.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function getProject(int $id)
    {
        $resource = $this->guzzle->get("//behance.com/v2/projects/{$id}?api_key={$this->token}");

        return \GuzzleHttp\json_decode($resource->getBody()->getContents());
    }
}