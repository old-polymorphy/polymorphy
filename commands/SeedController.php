<?php

namespace app\commands;

use yii\console\Controller;
use app\db\seeds\{
    PostsSeeder,
    TagsSeeder,
    PostTagSeeder,
    AdminSeeder,
    PortfolioSeeder
};

/**
 * Seeding console command.
 * @package app\commands
 */
class SeedController extends Controller
{
    public $message = 'Default message';

    /**
     * Runs registered seeders.
     * @throws \Exception
     */
    public function actionRun(): void
    {
        (new PostsSeeder(20))->run();
        (new TagsSeeder(10))->run();
        (new PostTagSeeder(30))->run();
        (new AdminSeeder(1))->run();
        (new PortfolioSeeder(10))->run();
    }
    
    /**
     * Sends the message set by default.
     */
    public function actionIndex()
    {
        echo $this->message . "\n";
    }
}