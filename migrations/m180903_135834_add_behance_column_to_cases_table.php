<?php

use yii\db\Migration;

/**
 * Handles adding behance to table `cases`.
 */
class m180903_135834_add_behance_column_to_cases_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cases', 'behance', $this->integer()->defaultValue(null));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('cases', 'behance');
    }
}
