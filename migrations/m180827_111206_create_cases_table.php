<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cases`.
 */
class m180827_111206_create_cases_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cases', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'body' => $this->text(),
            'image' => $this->string(),
            'link' => $this->string(),
            'type' => $this->tinyInteger(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cases');
    }
}
