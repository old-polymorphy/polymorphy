<?php

use yii\db\Migration;

/**
 * Handles the creation of table `post_tags`.
 */
class m180808_104307_create_post_tag_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('post_tag', [
            'id' => $this->primaryKey(),
            'post_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
        ]);

        // $this->addForeignKey(
        //     'fk-post_id',
        //     'post_tags',
        //     'post_id',
        //     'posts',
        //     'id',
        //     'CASCADE'
        // );

        // $this->addForeignKey(
        //     'fk-tag_id',
        //     'post_tags',
        //     'tag_id',
        //     'tags',
        //     'id',
        //     'CASCADE'
        // );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('post_tags');

        // $this->dropForeignKey(
        //     'fk-post_id',
        //     'post_tags'
        // );
    }
}
