<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tags`.
 */
class m180808_103848_create_tags_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tags', [
            'id' => $this->primaryKey(),
            'type' => $this->tinyInteger(),
            'text' => $this->string(16)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tags');
    }
}
