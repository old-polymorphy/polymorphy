<?php

use yii\db\Migration;

/**
 * Handles the creation of table `technologies`.
 */
class m180803_131807_create_technologies_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('technologies', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255),
            'type' => $this->tinyInteger(1),
            'image' => $this->binary(),
            'description' => $this->text()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('technologies');
    }
}
