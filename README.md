# Portfolio site

Regular portfolio. [Yii](https://yiiframework.com/) and [React.js](https://reactjs.org/) are used.

## Getting started
* Install (if you don't have them):
    * [Node.js](http://nodejs.org): `sudo apt-get install -y nodejs` on Ubuntu
    * [Composer](https://getcomposer.org) dependencies: `composer install`
    * JavaScript dependencies: `npm install`
* Run:
    * `docker-compose up -d` — runs containers
    * `npm run watch` — watches webpack files
* Learn:
    * `web/` root directory for nginx