<?php

namespace app\controllers;

// Yii
use Yii;
use yii\web\Controller;
use yii\helpers\Json;

// Forms
use app\models\forms\CallbackForm;
use app\models\forms\OrderForm;

/**
 * MailController class.
 */
class MailController extends Controller
{
    /**
     * Shows all posts.
     *
     * @return string
     */
    public function actionCallback()
    {
        $model = new CallbackForm();

        $request = Yii::$app->request;

        $model->load($request->post(), '');

        $result = $model->send();

        return Json::encode(['result' => $result]);
    }

    /**
     * Shows all posts.
     *
     * @return string
     */
    public function actionOrder()
    {
        $model = new OrderForm();

        $request = Yii::$app->request;

        $model->load($request->post(), '');

        $result = $model->send();

        return Json::encode(['result' => $result]);
    }
}