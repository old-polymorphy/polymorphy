<?php

namespace app\controllers;

// Yii
use app\models\forms\EditPostForm;
use function count;
use Exception;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\Controller;
use yii\helpers\Json;
use yii\web\Response;
use yii\web\UploadedFile;

// Models
use app\models\Post;
use app\models\forms\AddPostForm;

class PostController extends Controller
{
    public $layout = false;

    /**
     * Returns posts as models.
     *
     * @param null $year Year filter.
     * @param null $month Month filter.
     *
     * @return ActiveQuery
     */
    private function getAll($year = null, $month = null): ActiveQuery
    {
        $posts = Post::find()->with('tags')->with('user');

        if ($year) {
            $posts->andWhere(['extract(year from created_at)' => $year]);

            if ($month) {
                $posts->andWhere(['extract(month from created_at)' => $month]);
            }
        }

        return $posts;
    }

    /**
     * Renders the view.
     *
     * @return string
     */
    public function actionShow(): string
    {
        return $this->render('show.twig');
    }

    /**
     * Returns all posts in JSON.
     *
     * @param null $year Year filter.
     * @param null $month Month filter.
     *
     * @return array|ActiveRecord[]
     */
    public function actionAll($year = null, $month = null): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $posts = $this->getAll($year, $month);

        return $posts->asArray()->orderBy('created_at DESC')->all();
    }

    public function actionOne($id): string
    {
        return Json::encode(Post::find()->with('tags')->where(['id' => $id])->asArray()->one());
    }

    /**
     * Filters posts by tags.
     *
     * @return array
     */
    public function actionFilter(): array
    {
        $year = $_GET['year'] ?? null;   // Year
        $month = $_GET['month'] ?? null; // Month

        $posts = $this->actionAll($year, $month);

        $tags = str_replace('tags', '', $_GET);

        unset($tags['year'], $tags['month']);

        $tags = array_values($tags);

        // Filter posts
        $result = array_filter($posts, function ($post) use ($tags) {
            $postTags = array_map(function ($tag) {
                return $tag['id'];
            }, $post['tags']);

            return count(array_intersect($tags, $postTags)) === count($tags);
        });

        return array_values($result);
    }

    /**
     * Returns calendar.
     *
     * @return array
     */
    public function actionDates(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $dates = Post::find()->select('created_at')
            ->limit(10)->orderBy('created_at DESC')
            ->distinct()->all();

        return Post::generateCalendar($dates);
    }

    /**
     * Adds a post.
     *
     * @return false|string
     */
    public function actionAdd()
    {
        $model = new AddPostForm();
        $model->load(Yii::$app->request->post(), '');
        $model->file = UploadedFile::getInstanceByName('file');

        return Json::encode($model->persist());
    }

    /**
     * Edits the post.
     *
     * @param int $id — Numeric post ID.
     * @return string
     */
    public function actionEdit(int $id): string
    {
        $model = new EditPostForm();
        $model->load(Yii::$app->request->post(), '');
        $model->file = UploadedFile::getInstanceByName('file');
        $model->id = $id;

        return Json::encode($model->persist());
    }

    /**
     * @param int $id
     * @return bool|false|int|string
     * @throws \Throwable
     */
    public function actionDelete(int $id)
    {
        $post = (new Post())::findOne($id);

        try {
            if ($post) {
                return $post->delete();
            }
        } catch (Exception $exception) {
            return $exception->getMessage();
        }

        return false;
    }
}