<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;

class UserController extends Controller
{
    public $layout = false;

    /**
     * Returns all tags in JSON.
     *
     * @return bool|null|\yii\web\IdentityInterface
     */
    public function actionGetCurrent()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (Yii::$app->user->isGuest) {
            return false;
        }

        return Yii::$app->user->identity;
    }
}