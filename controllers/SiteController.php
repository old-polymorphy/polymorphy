<?php

namespace app\controllers;

// Yii
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;

// Models
use app\models\LoginForm;
use app\models\PortfolioCase;


/**
 * Class SiteController
 *
 * @package app\controllers
 */
class SiteController extends Controller
{
    public $layout = false;

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => yii\web\ErrorAction::class,
            ],
            'captcha' => [
                'class' => yii\captcha\CaptchaAction::class,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(): string
    {
        $cases = PortfolioCase::find()
            ->orderBy('id DESC')
            ->asArray()
            ->limit(4)
            ->all();

        return $this->render('home.twig', [
            'cases' => $cases,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout(): string
    {
        return $this->render('about');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        $post = Yii::$app->request->post();

        if ($post) {
            try {
                $model->load($post);
            } catch (\Exception $exception) {
                echo $exception->getMessage();
            }

            if ($model->login()) {
                return $this->goBack();
            }

            $model->password = '';
        }

        return $this->render('login.twig', [
            'model' => $model,
            'csrfParam' => Yii::$app->request->csrfParam,
            'csrfToken' => Yii::$app->request->csrfToken,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout(): Response
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
