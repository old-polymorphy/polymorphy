<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Tag;

class TagController extends Controller
{
    public $layout = false;

    /**
     * Fetches all tags from the database.
     *
     * @param int $type
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionAll($type = 1)
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $tags = Tag::find()->where(['type' => $type])->all();

        return $tags;
    }
}