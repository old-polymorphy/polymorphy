<?php

namespace app\controllers;

// Yii
use Yii;
use yii\web\Controller;

// Models
use app\models\Behance;
use app\models\PortfolioCase;

class PortfolioController extends Controller
{
    public $layout = false;

    /**
     * Renders the view.
     *
     * @return string
     */
    public function actionShow(): string
    {
        return $this->render('show.twig');
    }

    /**
     * Get all cases.
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGet(): array
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return PortfolioCase::find()->asArray()->all();
    }

    /**
     * Shows case page.
     *
     * @param $id
     *
     * @return string
     */
    public function actionCase($id): string
    {
        $case = PortfolioCase::findOne($id);

        $behance = $case && $case->getAttribute('behance') ?
            (new Behance())->getProject($case->getAttribute('behance')) :
            null;

        // Title
        $title = $behance ? $behance->project->name : $case->getAttribute('title');

        // Description
        $description = $case->getAttribute('body') ?: $behance->project->description;

        return $this->render('case.twig', [
            'case' => $case,
            'behance' => $behance,
            'title' => $title,
            'description' => $description,
        ]);
    }
}