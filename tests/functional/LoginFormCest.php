<?php

class LoginFormCest
{
    /**
     * Fires every time the test starts.
     *
     * @param FunctionalTester $I
     *
     * @return void
     */
    public function _before(\FunctionalTester $I): void
    {
        $I->amOnRoute('/login');
    }

    /**
     * Checks if the login page is opening.
     *
     * @param FunctionalTester $I
     *
     * @return void
     */
    public function openLoginPage(\FunctionalTester $I): void
    {
        $I->see('Авторизация', 'h1');
        $I->seeElement('.loginForm');
    }

    /**
     * Checks if a user is logged in by id.
     *
     * @param FunctionalTester $I
     *
     * @return void
     */
    public function internalLoginById(\FunctionalTester $I): void
    {
        $I->amLoggedInAs(1);
        $I->amOnPage('/');
        $I->see('Закажите сайт', 'h1');
    }

    /**
     * Checks if a user is logged in by name.
     *
     * @param FunctionalTester $I
     *
     * @return void
     */
    public function internalLoginByInstance(\FunctionalTester $I): void
    {
        $I->amLoggedInAs(\app\models\User::findByUsername('polymorphy'));
        $I->amOnPage('/');
        $I->see('Закажите сайт', 'h1');
    }

    /**
     * Checks if empty credentials are handled.
     *
     * @param FunctionalTester $I
     *
     * @return void
     */
    public function loginWithEmptyCredentials(\FunctionalTester $I): void
    {
        $I->submitForm('.loginForm', []);
        $I->expectTo('see validations errors');
        $I->see('Username cannot be blank.');
        $I->see('Password cannot be blank.');
    }


    /**
     * Checks if wrong credentials are handled.
     *
     * @param FunctionalTester $I
     *
     * @return void
     */
    public function loginWithWrongCredentials(\FunctionalTester $I): void
    {
        $I->submitForm('.loginForm', [
            'LoginForm[username]' => 'admin',
            'LoginForm[password]' => 'wrong',
        ]);
        $I->expectTo('see validations errors');
        $I->see('Incorrect username or password.');
    }


    /**
     * Checks if correct credentials are handled.
     *
     * @param FunctionalTester $I
     *
     * @return void
     */
    public function loginSuccessfully(\FunctionalTester $I): void
    {
        $I->submitForm('.loginForm', [
            'LoginForm[username]' => $_ENV['YII_USER'],
            'LoginForm[password]' => $_ENV['YII_PASSWORD'],
        ], 'submitButton');
        $I->see('Закажите сайт', 'h1');
        $I->dontSeeElement('.loginForm');
    }
}