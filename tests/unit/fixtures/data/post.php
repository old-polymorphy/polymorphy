<?php

return [
    'post0' => [
        'title' => 'Iste ipsum ea quas dolorem neque aliquam aperiam.',
        'body' => 'Quisquam rerum dolor atque saepe quis quaerat veritatis. Nobis rerum dolores harum. Minus odit suscipit distinctio aut enim voluptatem. Sunt autem quo quae et voluptatem voluptatum sit.',
        'image' => 'https://lorempixel.com/100/100/cats/?58047',
    ],
    'post1' => [
        'title' => 'Id quia ut reiciendis.',
        'body' => 'Ratione quasi neque et officiis. Temporibus voluptatem veniam necessitatibus quia nihil. Voluptas molestiae repellendus atque tempora consequatur eos quibusdam nulla.',
        'image' => 'https://lorempixel.com/100/100/cats/?44434',
    ],
    'post2' => [
        'title' => 'Totam deserunt nam officia molestiae laudantium incidunt laudantium.',
        'body' => 'Quia ducimus ad similique magnam. Quod doloremque dolor ut consequatur. Necessitatibus expedita saepe consequatur mollitia blanditiis.',
        'image' => 'https://lorempixel.com/100/100/cats/?83245',
    ],
    'post3' => [
        'title' => 'Sequi sit nulla et molestias labore explicabo sit.',
        'body' => 'Dolores maiores odit qui aut. Aspernatur quidem laudantium corrupti. Magnam illum dolorem vel. Qui facilis sed laborum minima. Maxime tempore voluptatum qui et ut accusamus. Ut illum consequatur ea.',
        'image' => 'https://lorempixel.com/100/100/cats/?29401',
    ],
    'post4' => [
        'title' => 'Sit aliquam necessitatibus recusandae ut omnis quia impedit.',
        'body' => 'Quo dolore reiciendis id nisi eum ut. Aspernatur aut similique dicta doloremque quod. Ad suscipit veritatis molestiae vitae sequi. Quod architecto quod dolorem dolorem cum natus.',
        'image' => 'https://lorempixel.com/100/100/cats/?29208',
    ],
    'post5' => [
        'title' => 'Quia tempora iusto consequuntur repudiandae est fuga est et.',
        'body' => 'Aliquid nemo id quaerat similique qui fugiat. Voluptatibus ut vel sit dolores natus voluptatibus. Maiores omnis laudantium temporibus sapiente quisquam nemo.',
        'image' => 'https://lorempixel.com/100/100/cats/?15877',
    ],
    'post6' => [
        'title' => 'Consectetur a quaerat repellendus distinctio expedita dolorum culpa corrupti.',
        'body' => 'Aspernatur corrupti excepturi et odit animi. Velit voluptate harum eaque quod rerum at esse. Nihil dolore quam dicta illum laborum.',
        'image' => 'https://lorempixel.com/100/100/cats/?26154',
    ],
];
