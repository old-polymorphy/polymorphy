<?php

return [
    'title' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    'body' => $faker->text($maxNbChars = 200),
    'image' => $faker->imageUrl('100', '100', 'cats')
];