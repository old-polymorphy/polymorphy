<?php

class LoginCest
{
    public function ensureThatLoginWorks(AcceptanceTester $I): void
    {
        $I->amOnPage('/login');
        $I->see('Авторизация', 'h1');

        $I->amGoingTo('try to login with correct credentials');
        $I->fillField('input[name="LoginForm[username]"]', $_ENV['YII_USER']);
        $I->fillField('input[name="LoginForm[password]"]', $_ENV['YII_PASSWORD']);
        $I->click('button.button');
        $I->wait(2);

        $I->expectTo('see the main page');
        $I->see('Закажите сайт', 'h1');
    }
}
