<?php

use yii\db\pgsql\Schema;
use yii\db\Connection;

return [
    'class' => Connection::class,
    'dsn' => env('DB_DRIVER') . ':host='
        . env('DB_HOST')
        . ';dbname=' . env('DB_DATABASE'),
    'username' => env('DB_USER'),
    'password' => env('DB_PASSWORD'),
    'charset' => 'utf8',
    'schemaMap' => [
        'pgsql' => [
            'class' => Schema::class,
            'defaultSchema' => 'public',
        ],
    ],

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];