<?php
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/test_db.php';
$router = require __DIR__ . '/router.php';

/**
 * Application configuration shared by all test types
 */
return [
    'id' => 'basic-tests',
    'basePath' => dirname(__DIR__),
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'language' => 'en-US',
    'components' => [
        'view' => [
            'class' => yii\web\View::class,
            'renderers' => [
                'twig' => [
                    'class' => \yii\twig\ViewRenderer::class,
                    'options' => [
                        'cache' => false,
                        'debug' => true,
                        'auto_reload' => true,
                    ],
                    'globals' => [
                        'html' => ['class' => \yii\helpers\Html::class],
                    ],
                    'uses' => ['yii\bootstrap'],
                    'filters' => [
                        'dump' => 'var_dump',
                    ],
                ],
            ],
        ],
        'db' => $db,
        'mailer' => [
            'useFileTransport' => true,
        ],
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'user' => [
            'identityClass' => app\models\User::class,
        ],
        'request' => [
            'cookieValidationKey' => 'test',
            'enableCsrfValidation' => false,
        ],

        'urlManager' => $router,
    ],
    'params' => $params,
];
