<?php
$db = require __DIR__ . '/db.php';
// test database! Important not to run tests on production or development databases
$db['dsn'] = env('DB_DRIVER') . ':host='
    . env('DB_HOST')
    . ';dbname=' . env('DB_DATABASE');

return $db;
