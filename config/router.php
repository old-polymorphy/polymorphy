<?php

return [
    'class' => yii\web\UrlManager::class,
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        'api/user' => 'user/get-current',
        'login' => 'site/login',
        'logout' => 'site/logout',
        'api/dates' => 'post/dates',
        'api/tags/<type:\d{1}>' => 'tag/all',

        'POST callback' => 'mail/callback',
        'POST order' => 'mail/order',

        // Portfolio
        'portfolio' => 'portfolio/show',
        'portfolio/case/<id:[0-9]+>/' => 'portfolio/case',
        'api/portfolio' => 'portfolio/get',

        // Posts
        'posts' => 'post/show',
        'posts/<year:\d{4}>' => 'post/show',
        'posts/<year:\d{4}>/<month:\d{1,2}>' => 'post/show',
        'POST posts/add' => 'post/add',

        // Posts (API)
        'api/posts' => 'post/all',
        'api/post/<id:\d+>' => 'post/one',
        'api/posts/<year:\d{4}>' => 'post/all',
        'api/posts/<year:\d{4}>/<month:\d{1,2}>' => 'post/all',
        'api/posts/?' => 'post/filter',
        'api/posts/<year:\d{4}>/?' => 'post/filter',
        'api/posts/<year:\d{4}>/<month:\d{1,2}>/?' => 'post/filter',
        'api/posts/delete/<id:\d+>' => 'post/delete',
        'POST api/posts/edit/<id:\d+>' => 'post/edit',
    ],
];