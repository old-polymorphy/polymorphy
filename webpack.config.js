const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const safeParser = require('postcss-safe-parser');

module.exports = {
  mode: 'development',
  entry: {
    'javascript/app': './src/javascript/script.js',
    'javascript/pages/post/show': './src/javascript/pages/post/show.js',
    'javascript/pages/portfolio/show': './src/javascript/pages/portfolio/show.js'
  },
  output: {
    path: path.resolve(__dirname, 'web'),
    filename: '[name].js'
  },
  module: {
    rules: [{
      test: /\.sass$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: ['css-loader?url=false', 'sass-loader']
      })
    },
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['env', 'react'],
          plugins: ['transform-class-properties', 'transform-object-rest-spread']
        }
      }]
  },
  resolve: {
    extensions: ['*', '.js', '.jsx'],
  },
  plugins: [
    new ExtractTextPlugin('css/style.css'),
    new OptimizeCssAssetsPlugin({
      cssProcessorOptions: {
        parser: safeParser,
        discardComments: {
          removeAll: true
        }
      }
    })
  ],
  stats: {
    warnings: false,
    chunks: false,
    entrypoints: false,
    children: false
  }
};