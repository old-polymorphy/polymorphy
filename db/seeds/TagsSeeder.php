<?php

namespace app\db\seeds;

use app\db\seeds\DatabaseSeeder;
use \tebazil\yii2seeder\Seeder;

/**
 * Tags seeding class.
 */
class TagsSeeder extends DatabaseSeeder
{    
    protected $table = 'tags';

    /**
     * {@inheritdoc}
     */
    protected function rules(): array
    {
        return [
            'type' => $this->faker->numberBetween($min = 1, $max = 2),
            'text' => $this->faker->word($maxNbChars = 16)
        ];
    }
}