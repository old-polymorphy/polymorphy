<?php

namespace app\db\seeds;

use \tebazil\yii2seeder\Seeder;

/**
 * Abstract seeding class.
 * @package app\db\seeds
 */
abstract class DatabaseSeeder
{
    public $seeder;
    
    protected $quantity;
    protected $table;
    protected $generator;
    protected $faker;

    /**
     * Class constructor.
     */
    public function __construct(int $quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Runs seeder.
     * @throws \Exception
     */
    public function run(): void
    {
        $this->seeder = new Seeder();
        $this->generator = $this->seeder->getGeneratorConfigurator();
        $this->faker = $this->generator->getFakerConfigurator();

        $this->seeder->table($this->table)->columns(
            $this->rules()
        )->rowQuantity($this->quantity);

        $this->seeder->refill();
    }

    /**
     * Sets seeding rules.
     */
    abstract protected function rules(): array;
}