<?php

namespace app\db\seeds;

use app\db\seeds\DatabaseSeeder;
use \tebazil\yii2seeder\Seeder;

/**
 * Posts seeding class.
 */
class PostTagSeeder extends DatabaseSeeder
{    
    protected $table = 'post_tag';

    /**
     * {@inheritdoc}
     */
    protected function rules(): array
    {
        return [
            'post_id' => $this->faker->numberBetween($min = 1, $max = 20),
            'tag_id' => $this->faker->numberBetween($min = 1, $max = 10),
        ];
    }
}