<?php

namespace app\db\seeds;

use \Yii;

use app\db\seeds\DatabaseSeeder;
use \tebazil\yii2seeder\Seeder;

/**
 * Admin seeding class.
 */
class PortfolioSeeder extends DatabaseSeeder
{    
    protected $table = 'cases';

    /**
     * {@inheritdoc}
     */
    protected function rules(): array
    {
        return [
            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'body' => $this->faker->text($maxNbChars = 200),
            'image' => $this->faker->imageUrl('640', '640', 'cats'),
            'link' => 'http://polymorphy.ru',
            'behance' => '',
            'type' => $this->faker->numberBetween($min = 1, $max = 3),
        ];
    }
}