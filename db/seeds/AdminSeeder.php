<?php

namespace app\db\seeds;

use \Yii;

use app\db\seeds\DatabaseSeeder;
use \tebazil\yii2seeder\Seeder;

/**
 * Admin seeding class.
 */
class AdminSeeder extends DatabaseSeeder
{    
    protected $table = 'users';

    /**
     * {@inheritdoc}
     */
    protected function rules(): array
    {
        return [
            'username' => env('YII_USER'),
            'password' => Yii::$app->getSecurity()->generatePasswordHash(env('YII_PASSWORD')),
            'auth_key' => Yii::$app->security->generateRandomString(),
            'access_token' => Yii::$app->security->generateRandomString(),
            'type' => 4
        ];
    }
}