<?php

namespace app\db\seeds;

use app\db\seeds\DatabaseSeeder;
use \tebazil\yii2seeder\Seeder;

/**
 * Posts seeding class.
 */
class PostsSeeder extends DatabaseSeeder
{    
    protected $table = 'posts';

    /**
     * {@inheritdoc}
     */
    protected function rules(): array
    {
        return [
            'title' => $this->faker->sentence($nbWords = 6, $variableNbWords = true),
            'body' => $this->faker->text($maxNbChars = 200),
            'image' => $this->faker->imageUrl('640', '640', 'cats'),
            'user_id' => 1,
            'created_at' => $this->faker->iso8601()
        ];
    }
}